<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('logout_auth', function(){
    auth()->logout();
    return redirect()->route('login');
});

Route::group(
    [

        'middleware' => ['web', 'auth', 'admin']
    ],
    function(){

        Route::get('/home', 'HomeController@index')->name('home');

        Route::get('/', 'TasksController@index');

        Route::get('get-tasks', 'TasksController@getTasks');

        Route::get('get-task/{id}', 'TasksController@getTaskId');

        Route::get('tasks-add', 'TasksController@add');

        Route::post('tasks-create', 'TasksController@create');

        Route::get('task-edit/{id}', 'TasksController@edit');

        Route::post('tasks-update', 'TasksController@update');

        Route::get('task-delete/{id}', 'TasksController@remove');
});