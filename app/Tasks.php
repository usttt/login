<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table = 'tasks';

    protected $fillable = ['name', 'to_do', 'status'];

    public function getAll()
    {
        $tasks = $this->get();

        return $tasks;
    }

    public function getId($id)
    {
        $task = $this->find($id);

        return $task;
    }

    public function store($request)
    {
        $this->name = $request['name'];
        $this->to_do = $request['task'];
        $this->save();

        return true;
    }

    public function taskUpdate($request)
    {
        $task = $this->find($request['id']);

        if(is_null($task))
        {
            return false;
        }

        $task->name         = $request['name'];
        $task->to_do        = $request['task'];
        $task->status       = $request['status'];
        $task->updated_at   = now();
        $task->save();

        return true;
    }

    public function remove($id)
    {
        $task = $this->find($id);

        if(is_null($task))
        {
            return false;
        }
        
        $task->delete($id);

        return true;
  
    }
}
