@extends('partials.layouts')
@section('content')

<div class="container"  >
    <div class="row">
        <div class="col-4">
            <h1 class="mt-4">Добавить задачу</h1>
            <form>
                <div class="form-group">
                    <label for="name">Название задачи</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Название" >
                </div>
                <div class="form-group">
                    <label for="task">Задача</label>
                    <textarea class="form-control" name="task" id="task" rows="5"></textarea>
                </div>

                <button type="button" class="btn btn-primary float-left" id="create_data">Добавить</button>
                <a href="{{url('/')}}" class="btn btn-secondary float-right">Назад</a>
            </form>
            <br />
            <div id="alert_message">
            
                {{--<div class="alert alert-danger mt-4" role="alert">
                    dsadasf
                </div>--}}
            </div>
            
        </div>
    </div>

</div>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#create_data").click(function(){
        
        $.ajax({
            type:'POST',
            url:'/tasks-create',
            data: $("input[type = 'text'], textarea"),
            dataType: 'json',
            
            success: function(data) {
                
                if($('.alert-danger').length > 0)
                {
                    $('.alert-danger').remove();
                }

                if($('.alert-success').length > 0)
                {
                    $('.alert-success').remove();
                }
                
                if(data.message.success)
                {        
                    $('#alert_message').append('<div class="alert alert-success mt-4" role="alert">'+data.message.success+'</div>');
                }
                if(data.message.errors)
                {
                    $.each(data.message.errors, function(index, item) {      
                        
                        $('#alert_message').append('<div class="alert alert-danger mt-4" role="alert">'+item+'</div>');
                        
                    });
                }
            }
        });
    });
    
</script>
@endsection