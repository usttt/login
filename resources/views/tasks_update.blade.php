@extends('partials.layouts')
@section('content')

<div class="container"  >
    <div class="row">
        <div class="col-4">
            <h1 class="mt-4">Изменить задачу</h1>
            <form>
                <div class="form-group">
                    <label for="name">Название задачи</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Название" >
                </div>
                <div class="form-group">
                    <label for="task">Задача</label>
                    <textarea class="form-control" name="task" id="task" rows="5"></textarea>
                </div>

                <div class="form-group">
                    <label for="status">Статус</label>
                    <select name="status" id="status" class="form-control">
                        <option value="0">В процессе</option>
                        <option value="1">Выполнено</option>
                    </select>
                </div>

                <input type="hidden" name="id" id="task_id" value="{{$task->id}}" />
                
                <button type="button" class="btn btn-primary float-left" id="update_data">Изменить</button>
                <a href="{{url('/')}}" class="btn btn-secondary float-right">Назад</a>
                
            </form>
            <br />
            <div id="alert_message">
            
            </div>
            
        </div>
    </div>

</div>

<script>
    
    $.ajax({
        type:'GET',
        url:'{!!url("get-task") !!}/'+$("#task_id").val(),
        dataType: 'json',

        success: function (data) {
            $("#name").val(data.data.name);
            $("#task").val(data.data.to_do);
            $("#status").val(data.data.status);
        }
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#update_data").click(function(){
        
        $.ajax({
            type:'POST',
            url:'/tasks-update',
            data: $("input[type = 'text'], input[type = 'hidden'], select, textarea"),
            dataType: 'json',
            
            success: function(data) {
                
                if($('.alert-danger').length > 0)
                {
                    $('.alert-danger').remove();
                }

                if($('.alert-success').length > 0)
                {
                    $('.alert-success').remove();
                }
                
                if(data.message.success)
                {         
                    $('#alert_message').append('<div class="alert alert-success mt-4" role="alert">'+data.message.success+'</div>');
                }
                if(data.message.errors)
                {
                    $.each(data.message.errors, function(index, item) {      
                        
                        $('#alert_message').append('<div class="alert alert-danger mt-4" role="alert">'+item+'</div>');
                        
                    });
                }
            }
        });
    });
    
</script>
@endsection