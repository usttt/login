<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Tasks;

class LoginProject extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::updateOrCreate([
            'email' => 'admin@gmail.com',
        ],
            [
                'name' => 'Administrator',
                'email' => 'admin@gmail.com',
                'role'  =>  2,
                'password' => Hash::make('admin'),
        ]);

        $user = User::updateOrCreate([
            'email' => 'client@gmail.com',
        ],
            [
                'name' => 'Client',
                'email' => 'client@gmail.com',
                'role'  =>  1,
                'password' => Hash::make('client'),
        ]);

        $task = Tasks::updateOrCreate([
            'name' => 'Задача 1',
        ],
            [
                'name' => 'Задача 1',
                'to_do' => 'Создать отчеты',
        ]);

        $task = Tasks::updateOrCreate([
            'name' => 'Задача 2',
        ],
            [
                'name' => 'Задача 2',
                'to_do' => 'Составить график',
        ]);

        $task = Tasks::updateOrCreate([
            'name' => 'Задача 3',
        ],
            [
                'name' => 'Задача 3',
                'to_do' => 'Провести собрание',
        ]);

    }
}
